#!/bin/env python3

import http.server
from os.path import join as pjoin
import logging
import uuid
import os
import time
import threading
from datetime import datetime, timedelta
import urllib.parse

storage_path = os.getenv("STORAGE_PATH", default="/tmp/pypaste/")
max_age_days = int(os.getenv("MAX_AGE_DAYS", default="7"))
if not os.path.exists(storage_path):
    os.makedirs(storage_path)


def id_generator(folder):
    """ "Returns an available short random file name inside the folder."""
    while True:
        file_name = str(uuid.uuid4())[:4]
        if not os.path.isfile(f"{folder}/{file_name}"):
            return file_name


class Handler(http.server.SimpleHTTPRequestHandler):
    def reply(self, status, body):
        output = str.encode(str(body))
        self.send_response(int(status))
        self.send_header("Content-type", "text/plain; charset=UTF-8")
        self.end_headers()
        self.wfile.write(output)

    def do_GET(self):
        if self.path == "/":
            index_path = pjoin(os.curdir, "index.html")
            with open(index_path) as fh:
                self.send_response(200)
                self.send_header("Content-type", "text/html")
                self.end_headers()
                self.wfile.write(fh.read().encode())
        else:
            paste_id = self.path[1:]
            store_path = pjoin(storage_path, paste_id)

            if not os.path.isfile(store_path):
                self.reply(404, "paste id not found")
                return
            
            with open(store_path) as fh:
                self.send_response(200)
                self.send_header("Content-type", "text/plain")
                self.end_headers()
                self.wfile.write(fh.read().encode())
        return

    def do_POST(self):
        if self.path != "/":
            self.reply(400, "only POST on / plz")
        else:
            content_length = int(self.headers.get('Content-Length', 0))
            post_data = self.rfile.read(content_length).decode('utf-8')
            form_data = urllib.parse.parse_qs(post_data)
            
            if 'content' not in form_data:
                self.reply(400, "missing content field")
                return
                
            data = form_data['content'][0]
            paste_id = id_generator(storage_path)
            store_path = os.path.join(storage_path, paste_id)
            with open(store_path, "w") as fh:
                fh.write(data)
            self.send_response(302)
            self.send_header("Location", f"/{paste_id}")
            self.end_headers()
        return


def cleanup_old_pastes():
    """Remove paste files older than max_age_days"""
    while True:
        try:
            current_time = time.time()
            max_age_seconds = max_age_days * 24 * 60 * 60
            removed = 0
            
            for filename in os.listdir(storage_path):
                file_path = os.path.join(storage_path, filename)
                if not os.path.isfile(file_path):
                    continue
                    
                if (current_time - os.path.getmtime(file_path)) > max_age_seconds:
                    try:
                        os.remove(file_path)
                        removed += 1
                    except OSError as e:
                        logging.error(f"Error deleting {filename}: {e}")
            
            if removed > 0:
                logging.info(f"Cleanup completed. Removed {removed} old paste(s)")
        except Exception as e:
            logging.error(f"Error during cleanup: {e}")
        
        # Sleep for 1 hour and a bit before next cleanup check
        time.sleep(3666)


def run(server_class=http.server.ThreadingHTTPServer, handler_class=Handler):
    server_address = ("0.0.0.0", 8080)
    httpd = server_class(server_address, handler_class)
    
    # Start cleanup thread
    cleanup_thread = threading.Thread(target=cleanup_old_pastes, daemon=True)
    cleanup_thread.start()
    logging.info('Started cleanup thread')
    
    httpd.serve_forever()


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s'
    )
    logging.info('Starting up...')
    run()
    logging.info('Exiting')