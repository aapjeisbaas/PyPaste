# place file in > $HOME/.oh-my-zsh/custom/PyPaste.zsh
# PyPaste https://gitlab.com/aapjeisbaas/PyPaste https://p.steinvanbroekhoven.nl/
pp() { curl -s -F "content=<${1--}" -F ttl=604800 -w "%{redirect_url}\n" -o /dev/null https://p.steinvanbroekhoven.nl/; }