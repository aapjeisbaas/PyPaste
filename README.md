Inspired by 6paster I tried to build a small standalone python version.

It uses uuid4 named files stored in the `STORAGE_PATH` (defaults to /tmp/pypaste/) as "backend" and has no external database.

## usage
Add a function to your shell to upload to PyPaste
```
# ZSH

# create the plugin folder
mkdir -p $ZSH/custom/plugins/pp
# download the plugin
curl -o $ZSH/custom/plugins/pp/pp.plugin.zsh https://gitlab.com/aapjeisbaas/PyPaste/-/raw/main/PyPaste.zsh
# add the plugin to the plugins list
sed -i '/plugins=/ {/pp/! s/plugins=(\([^)]\+\))/plugins=(\1 pp)/}' ~/.zshrc

```

Use it to pipe data in:
```
kubectl get all | pp
lscpu | pp
docker info | pp
sudo dmesg | pp
```

Use it to upload a file
```
pp /var/log/my-cool-log-file.log
```


## Run it yourself
just to try out
```
docker run -p 8080:8080 registry.gitlab.com/aapjeisbaas/pypaste:latest
```

with persistent storage
```
mkdir $HOME/pypaste
docker run -p 8080:8080 -v ${HOME}/pypaste:/mnt/pypaste -e STORAGE_PATH='/mnt/pypaste/' registry.gitlab.com/aapjeisbaas/pypaste:latest
```

in k8s with rook ceph as storage
```
kubectl apply -f https://gitlab.com/aapjeisbaas/PyPaste/-/raw/main/k8s-manifest.yaml
```

